#ifndef DEBTMANAGER_H
#define DEBTMANAGER_H

#include <QObject>
#include <QMap>
#include "debtitem.h"
#include "debtmodel.h"
#include "debtpersisthandler.h"

class DebtManager : public QObject
{
    Q_OBJECT
public:
    explicit DebtManager(QObject *parent, DebtModel * model);
    ~DebtManager();
    Q_INVOKABLE void addDebt( const QString & name, const QString & color, const QString & loanAmount, const QString & minPayment, const QString & interestRate );
    Q_INVOKABLE void removeRows( const int &index);
    Q_INVOKABLE void editDebt( const int &index, const QString & name, const QString & color, const QString & loanAmount, const QString & minPayment, const QString & interestRate );
    Q_INVOKABLE void makePayment( const int &index, const QString &paymentAmt);
    Q_INVOKABLE int debtItemCardCount( const QString &name);
    Q_INVOKABLE QString debtAmount( const QString & name);
    Q_INVOKABLE QString interestRate( const QString & name);
    Q_INVOKABLE QString minPayment( const QString & name);
signals:

public slots:

private:
    void LoadDebtList();
    DebtModel * m_debtModel;
    QMap< QString, DebtItem*> _nameToDebtItemMap;
    DebtPersistHandler persistHandler;

};

#endif // DEBTMANAGER_H
