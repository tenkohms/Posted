#include "debtmodel.h"
#include <QDebug>

DebtDisplay::DebtDisplay(const QString &name, const QString &color, const int &cards)
    : m_name( name ),
      m_color( color ),
      m_cards( cards )
{
}

QString DebtDisplay::name() const
{
    return m_name;
}

QString DebtDisplay::color() const
{
    return m_color;
}

int DebtDisplay::cards() const
{
    return m_cards;
}

DebtModel::DebtModel( QObject *parent )
    : QAbstractListModel( parent )
{

}

void DebtModel::addDebt(const DebtDisplay &debt)
{
    beginInsertRows( QModelIndex(), rowCount(), rowCount() );
    m_debt << debt;
    endInsertRows();
}

void DebtModel::removeDebt(const int &index)
{
    beginRemoveRows(QModelIndex(), index, index); // no parent, one row to remove

    m_debt.removeAt(index);

    endRemoveRows();
}

void DebtModel::editDebt(const int &index, const QString &name, const QString &color, const int &cards)
{
    DebtDisplay old = m_debt[index];
//    qDebug() << old.name() << old.color() << old.cards();
//    qDebug() << name << color << cards;
    m_debt[ index ] = DebtDisplay( name, color, cards );
}

QString DebtModel::debtName(const int &index)
{
    return m_debt[index].name();
}

int DebtModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED( parent );
    return m_debt.count();
}

QVariant DebtModel::data(const QModelIndex &index, int role) const
{
    if ( index.row() < 0 || index.row() >= m_debt.count() )
        return QVariant();

    const DebtDisplay &debt = m_debt[index.row()];
    if ( role == NameRole )
        return debt.name();
    else if ( role == ColorRole )
        return debt.color();
    else if ( role == CardsRole )
        return debt.cards();
    else
        return QVariant();
}

QHash< int, QByteArray > DebtModel::roleNames() const {
    QHash< int, QByteArray> roles;
    roles[ NameRole ] = "name";
    roles[ ColorRole ] = "color";
    roles[ CardsRole ] = "cards";
    return roles;
}
