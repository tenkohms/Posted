#include "settingshandler.h"
#include <QDebug>

SettingsHandler::SettingsHandler(QObject *parent) : QObject(parent)
{
    theSettings = new QSettings( "LowRentGames", "Posted" );
}

SettingsHandler::~SettingsHandler()
{
    delete theSettings;
}

bool SettingsHandler::ValueExists(QString Key)
{
    return theSettings->contains( Key );
}

QVariant SettingsHandler::GetValue(QString Key)
{
    return theSettings->value( Key );
}

void SettingsHandler::SetValue(QString Key, QVariant Value)
{
    theSettings->setValue( Key, Value );
}

void SettingsHandler::ClearValue(QString Key)
{
    if ( theSettings->contains(Key) )
    {
        theSettings->remove( Key );
    }
}
