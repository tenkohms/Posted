#ifndef SETTINGSHANDLER_H
#define SETTINGSHANDLER_H

#include <QObject>
#include <QSettings>

class SettingsHandler : public QObject
{
    Q_OBJECT
public:
    static SettingsHandler& getInstance() {
        static SettingsHandler settingsHandler;
        return settingsHandler;
    }
    ~SettingsHandler();

    bool ValueExists( QString Key );
    QVariant GetValue( QString Key );
    void SetValue( QString Key, QVariant Value );
    void ClearValue( QString Key );

signals:

public slots:

private:
    SettingsHandler(QObject *parent = nullptr);
    SettingsHandler( SettingsHandler const& );
    void operator=(SettingsHandler const&);
    QSettings * theSettings;
};

#endif // SETTINGSHANDLER_H
