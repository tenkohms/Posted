import QtQuick 2.0
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

Item {
    ListView {
        id: listview
        clip: true
        anchors.top: parent.top
        anchors.topMargin: 10
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        model: debtCardModel
        spacing: 20
        delegate: DebtCardDelegate {}
    }

    Button {
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        anchors.margins: 10
        highlighted: true
        text: "+"
        onClicked: addDebt.visible = true
    }

    AddDebtWindow {
        id: addDebt
        visible: false
        anchors.fill: parent
        onAddDebtSignal: debtManager.addDebt( name, color, loanAmount, minPayment, interestRate )
    }
}
