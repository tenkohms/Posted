import QtQuick 2.0
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0

Drawer {
    id: drawer

    Rectangle {
        id: drawerHeader
        anchors.top: drawer.top
        anchors.left: drawer.left
        height: drawer.height / 4
        width: drawer.width
        color: "#4CAF50"

        Image {
            anchors.centerIn: parent
            height: parent.height / 1.2
            width: height
            source: "images/moneyCartoon.png"
            fillMode: Image.PreserveAspectFit
        }
    }

    DrawerButton {
        id: homeButton
        anchors.top: drawerHeader.bottom
        anchors.topMargin: 10
        imageSource: "images/homeIcon.png"
        buttonText: "Home"
    }

    DrawerButton {
        id: forecastButton
        anchors.top: homeButton.bottom
        anchors.topMargin: 10
        imageSource: "images/moneyCloud.png"
        buttonText: "Financial Forecast"
    }

    DrawerButton {
        id: settingsButton
        anchors.top: forecastButton.bottom
        anchors.topMargin: 10
        imageSource: "images/settingsIcon.png"
        buttonText: "Settings"
    }
}
