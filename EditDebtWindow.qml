import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Dialogs 1.2

Rectangle {
    id: editDebt
    visible: false
    anchors.fill: parent
    color: Qt.rgba( 0, 0, 0, .5)

    onVisibleChanged: {
        if ( visible ) {
            editDebtEntryWindow.height = height * .75
            editDebtEntryWindow.width = width * .75
        }
        else {
            editDebtEntryWindow.height = 0
            editDebtEntryWindow.width = 0
        }
    }

    ColorDialog {
        id: colorPicker
        visible: false
        currentColor: editDebtEntryWindow.colorPickerColor
        onColorChanged: editDebtEntryWindow.colorPickerColor = color
    }

    Rectangle {
        id: editDebtEntryWindow
        anchors.centerIn: parent
        radius: 5
        property string colorPickerColor: root.currentDebtColor

        Label {
            id: nameLabel
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 20
            text: qsTr("Name:")
        }

        TextField {
            id: nameInput
            anchors.top: nameLabel.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 20
            placeholderText: qsTr("Loan Name")
            text: root.currentDebtName
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: loanAmountLabel
            anchors.top: nameInput.bottom
            anchors.left: parent.left
            anchors.margins: 20
            text: qsTr("Loan Amount:")
        }

        TextField {
            id: loanAmountInput
            anchors.top: loanAmountLabel.bottom
            anchors.left: parent.left
            width: parent.width / 2
            anchors.margins: 20
            validator: DoubleValidator { bottom: 00.00;
                decimals: 2;
                notation: DoubleValidator.StandardNotation }
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            text: debtManager.debtAmount( root.currentDebtName )
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: interestRateLabel
            anchors.top: loanAmountInput.bottom
            anchors.left: parent.left
            anchors.margins: 20
            text: qsTr("Monthly Interest Rate:")
        }

        TextField {
            id: interestRateInput
            anchors.top: interestRateLabel.bottom
            anchors.left: parent.left
            anchors.margins: 20
            width: loanAmountInput.width / 2
            validator: DoubleValidator { bottom: 00.00;
                top: 99.99;
                decimals: 2;
                notation: DoubleValidator.StandardNotation }
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            text: debtManager.interestRate( root.currentDebtName )
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: interestSymbolLabel
            anchors.verticalCenter: interestRateInput.verticalCenter
            anchors.left: interestRateInput.right
            anchors.leftMargin: 10
            text: "%"
        }

        Label {
            id: minPaymentLabel
            anchors.top: interestRateInput.bottom
            anchors.left: parent.left
            anchors.margins: 20
            width: parent.width / 2
            text: qsTr("Payment Amount:")
        }

        TextField {
            id: minPaymentInput
            anchors.top: minPaymentLabel.bottom
            anchors.left: parent.left
            anchors.margins: 20
            validator: DoubleValidator { bottom: 00.00;
                decimals: 2;
                notation: DoubleValidator.StandardNotation }
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            text: debtManager.minPayment( root.currentDebtName )
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: colorLabel
            anchors.top: minPaymentInput.bottom
            anchors.left: parent.left
            anchors.margins: 20
            text: qsTr("Post-it Color:")
        }

        Rectangle {
            id: colorPickerSquare
            anchors.left: colorLabel.right
            anchors.leftMargin: 10
            anchors.verticalCenter: colorLabel.verticalCenter
            height: colorLabel.height
            width: height
            color: editDebtEntryWindow.colorPickerColor
            Material.elevation: 6
            MouseArea {
                anchors.fill: parent
                onClicked: colorPicker.visible = true
            }
        }

        Button {
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 10
            highlighted: true
            text: "Save"
            onClicked: {
                debtManager.editDebt( currentDebtIndex, nameInput.text, editDebtEntryWindow.colorPickerColor, loanAmountInput.text, minPaymentInput.text, interestRateInput.text )

                root.headerText = nameInput.text
                root.currentDebtName = nameInput.text
                root.currentDebtColor = editDebtEntryWindow.colorPickerColor
                root.currentDebtCardCount = debtManager.debtItemCardCount( root.currentDebtName )
                editDebt.visible = false
            }
        }

        Button {
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.margins: 10
            highlighted: true
            text: "Cancel"
            Material.accent: Material.Red
            onClicked: {
                editDebt.visible = false
            }
        }

        Behavior on height {

           NumberAnimation {
               duration: 500
               easing.type: Easing.OutBounce
           }
        }

        Behavior on width {

           NumberAnimation {
               duration: 500
               easing.type: Easing.OutBounce
           }
       }
    }
}
