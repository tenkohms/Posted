#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQuickStyle>
#include <QDebug>
#include <qqmlcontext.h>

#include "debtmodel.h"
#include "debtmanager.h"

int main(int argc, char *argv[])
{
#if defined(Q_OS_WIN)
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
#endif
    QGuiApplication app(argc, argv);

    QQuickStyle::setStyle("Material");

    DebtModel model;

    DebtManager manager( nullptr, &model );

    qmlRegisterUncreatableType<DebtManager>("Posted", 1, 0, "DebtManager", "");
    QQmlApplicationEngine engine;
    engine.rootContext()->setContextProperty("debtManager", &manager);
    QQmlContext * context = engine.rootContext();
    context->setContextProperty("debtCardModel", &model);
    engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
    if (engine.rootObjects().isEmpty())
        return -1;

    return app.exec();
}
