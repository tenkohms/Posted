#ifndef DEBTMODEL_H
#define DEBTMODEL_H
#include <QAbstractListModel>
#include <QString>

class DebtDisplay {
public:
    DebtDisplay( const QString & name, const QString & color, const int & cards );

    QString name() const;
    QString color() const;
    int cards() const;

private:
    QString m_name, m_color;
    int m_cards;
};


class DebtModel : public QAbstractListModel
{
    Q_OBJECT
public:
    enum DebtRoles {
        NameRole = Qt::UserRole +1,
        ColorRole,
        CardsRole
    };

    explicit DebtModel( QObject *parent = 0);

    void addDebt( const DebtDisplay &debt);

    void removeDebt( const int &index);

    void editDebt( const int &index, const QString &name, const QString &color, const int &cards);

    QString debtName( const int &index);

    int rowCount( const QModelIndex & parent = QModelIndex()) const;

    QVariant data(const QModelIndex & index, int role = Qt::DisplayRole) const;

protected:
   QHash<int, QByteArray> roleNames() const;

private:
   QList<DebtDisplay> m_debt;
};

#endif // DEBTMODEL_H
