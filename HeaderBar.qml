import QtQuick 2.0
import QtQuick.Controls 2.2
import QtGraphicalEffects 1.0

Rectangle {
    id: headerBar
    height: parent.height / 15
    color: "#4CAF50"

    property bool isBack: false

    Image {
        id: menuButton
        anchors.top: parent.top
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.margins: 10
        width: height

        source: {
            if ( isBack ){
                "images/backArrow.png"
            }
            else
            {
                "images/MenuButton.png"
            }
        }
        fillMode: Image.PreserveAspectFit
        MouseArea {
            anchors.fill: parent
            onClicked: {
                if ( isBack ) {
                    root.headerText = "ABCDEFGHIJKLM"
                    pageLoader.source = "DebtCardPage.qml"
                    isBack = false
                }
                else {
                    drawer.open()
                }
            }
        }
    }

    Label {
        anchors.left: menuButton.right
        anchors.leftMargin: 30
        anchors.verticalCenter: menuButton.verticalCenter
        height: menuButton.height
        //height: parent.height * .65
        width: parent.width
        color: "white"
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
        text: headerText
    }
}
