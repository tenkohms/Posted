import QtQuick 2.0
import QtQuick.Controls.Material 2.2
import QtQuick.Controls 2.2

Pane {
    property int debtPostitCellWidth: 30

    id: thisCard
    anchors.horizontalCenter: parent.horizontalCenter
    width: listview.width * .85
    height: listview.height / 5

    Material.elevation: 6

    GridView {
        anchors.top: cardName.bottom
        anchors.topMargin: 5
        anchors.bottom: parent.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        model: cards
        clip: true
        cellHeight: debtPostitCellWidth
        cellWidth: debtPostitCellWidth
        delegate: Pane {
            width: debtPostitCellWidth - 10
            height: debtPostitCellWidth - 10

            Material.elevation: 6
            Material.background: {
                color
            }
        }
    }

    Label {
        id: cardName
        anchors.top: parent.top
        anchors.topMargin: 5
        anchors.horizontalCenter: parent.horizontalCenter
        text: name

    }

    MouseArea {
        anchors.fill: parent
        onPressAndHold: debtManager.removeRows(index)
        onClicked: {
            headerBar.isBack = true
            root.currentDebtIndex = index
            root.currentDebtName = name
            root.headerText = name
            root.currentDebtColor = color
            root.currentDebtCardCount = cards
            pageLoader.source="DebtViewer.qml"
        }
    }
}
