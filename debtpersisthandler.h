#ifndef DEBTPERSISTHANDLER_H
#define DEBTPERSISTHANDLER_H

#include <QObject>
#include <debtitem.h>

class DebtPersistHandler : public QObject
{
    Q_OBJECT
public:
    explicit DebtPersistHandler(QObject *parent = nullptr);
    void saveDebtItem( DebtItem * debtItem );
    DebtItem* loadDebtItem( const QString &debtName );
    void deleteDebtItem( const QString &debtName );
    void updateDebtItem( DebtItem * debtItem );

signals:

public slots:
};

#endif // DEBTPERSISTHANDLER_H
