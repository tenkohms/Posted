import QtQuick 2.0
import QtQuick.Layouts 1.3
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2
import QtQuick.Dialogs 1.2

Rectangle {
    id: addDebt
    visible: false
    anchors.fill: parent
    color: Qt.rgba( 0, 0, 0, .5)

    signal addDebtSignal( string name, string color, string loanAmount, string minPayment, string interestRate )

    function closeAndReset()
    {
        addDebt.visible = false
        nameInput.text = ""
        newDebtEntryWindow.colorPickerColor = "#FFEB3B"
        loanAmountInput.text = ""
        minPaymentInput.text = ""
        interestRateInput.text = ""
    }

    onVisibleChanged: {
        if ( visible ) {
            newDebtEntryWindow.height = height * .75
            newDebtEntryWindow.width = width * .75
        }
        else {
            newDebtEntryWindow.height = 0
            newDebtEntryWindow.width = 0
        }
    }

    ColorDialog {
        id: colorPicker
        visible: false
        currentColor: newDebtEntryWindow.colorPickerColor
        onColorChanged: newDebtEntryWindow.colorPickerColor = color
    }

    Rectangle {
        id: newDebtEntryWindow
        anchors.centerIn: parent
        radius: 5
        property string colorPickerColor: "#FFEB3B"

        Label {
            id: nameLabel
            anchors.top: parent.top
            anchors.left: parent.left
            anchors.margins: 20
            text: qsTr("Name:")
        }

        TextField {
            id: nameInput
            anchors.top: nameLabel.bottom
            anchors.left: parent.left
            anchors.right: parent.right
            anchors.margins: 20
            placeholderText: qsTr("Loan Name")
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: loanAmountLabel
            anchors.top: nameInput.bottom
            anchors.left: parent.left
            anchors.margins: 20
            text: qsTr("Loan Amount:")
        }

        TextField {
            id: loanAmountInput
            anchors.top: loanAmountLabel.bottom
            anchors.left: parent.left
            width: parent.width / 2
            anchors.margins: 20
            validator: DoubleValidator { bottom: 00.00;
                decimals: 2;
                notation: DoubleValidator.StandardNotation }
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: interestRateLabel
            anchors.top: loanAmountInput.bottom
            anchors.left: parent.left
            anchors.margins: 20
            text: qsTr("Monthly Interest Rate:")
        }

        TextField {
            id: interestRateInput
            anchors.top: interestRateLabel.bottom
            anchors.left: parent.left
            anchors.margins: 20
            width: loanAmountInput.width / 2
            validator: DoubleValidator { bottom: 00.00;
                top: 99.99;
                decimals: 2;
                notation: DoubleValidator.StandardNotation }
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: interestSymbolLabel
            anchors.verticalCenter: interestRateInput.verticalCenter
            anchors.left: interestRateInput.right
            anchors.leftMargin: 10
            text: "%"
        }

        Label {
            id: minPaymentLabel
            anchors.top: interestRateInput.bottom
            anchors.left: parent.left
            anchors.margins: 20
            width: parent.width / 2
            text: qsTr("Payment Amount:")
        }

        TextField {
            id: minPaymentInput
            anchors.top: minPaymentLabel.bottom
            anchors.left: parent.left
            anchors.margins: 20
            validator: DoubleValidator { bottom: 00.00;
                decimals: 2;
                notation: DoubleValidator.StandardNotation }
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            horizontalAlignment: Text.AlignHCenter
        }

        Label {
            id: colorLabel
            anchors.top: minPaymentInput.bottom
            anchors.left: parent.left
            anchors.margins: 20
            text: qsTr("Post-it Color:")
        }

        Rectangle {
            id: colorPickerSquare
            anchors.left: colorLabel.right
            anchors.leftMargin: 10
            anchors.verticalCenter: colorLabel.verticalCenter
            height: colorLabel.height
            width: height
            color: newDebtEntryWindow.colorPickerColor
            Material.elevation: 6
            MouseArea {
                anchors.fill: parent
                onClicked: colorPicker.visible = true
            }
        }

        Button {
            anchors.right: parent.right
            anchors.bottom: parent.bottom
            anchors.margins: 10
            highlighted: true
            text: "Add"
            onClicked: {
                addDebt.addDebtSignal( nameInput.text, newDebtEntryWindow.colorPickerColor, loanAmountInput.text, minPaymentInput.text, interestRateInput.text)
                closeAndReset()
            }
        }

        Button {
            anchors.left: parent.left
            anchors.bottom: parent.bottom
            anchors.margins: 10
            highlighted: true
            text: "Cancel"
            Material.accent: Material.Red
            onClicked: {
                closeAndReset()
            }
        }

        Behavior on height {

           NumberAnimation {
               duration: 500
               easing.type: Easing.OutBounce
           }
        }

        Behavior on width {

           NumberAnimation {
               duration: 500
               easing.type: Easing.OutBounce
           }
       }
    }

}
