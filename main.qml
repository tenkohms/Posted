import QtQuick 2.9
import QtQuick.Window 2.2
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2

import Posted 1.0

Window {
    id: root
    visible: true
    width: 1080/2
    height: 1920
    title: qsTr("Posted")
//    flags: Qt.MaximizeUsingFullscreenGeometryHint

    Material.accent: Material.Green

    property string headerText: "ABCDEFGHIJKLM"
    property int currentDebtIndex: 0
    property string currentDebtName
    property string currentDebtColor
    property int currentDebtCardCount

    Rectangle {
        anchors.fill: parent
        color: "#e8e8e8"
    }

    HeaderBar {
        id: headerBar
        anchors.top: parent.top
        anchors.left: parent.left
        anchors.right: parent.right
    }

    AppDrawer {
        id: drawer
        width: 0.8 * root.width
        height: root.height
    }

    Loader {
        id: pageLoader
        anchors.top: headerBar.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.bottom: parent.bottom
        source: "DebtCardPage.qml"
    }
}
