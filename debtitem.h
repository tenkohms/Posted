#ifndef DEBTITEM_H
#define DEBTITEM_H

#include <QObject>

class DebtItem : public QObject
{
    Q_OBJECT
public:
    explicit DebtItem(QObject *parent = nullptr,
                      QString name = "",
                      QString color = "",
                      double loanAmt = 0.0,
                      double minPayment = 0.0,
                      double interestRate = 0.0);
    QString name();
    QString color();
    double loanAmt();
    double minPayment();
    double interestRate();

    void editMulti( const QString & name, const QString & color, const QString & loanAmount, const QString & minPayment, const QString & interestRate );

    void setName( const QString & );
    void setColor( const QString & );
    void setLoanAmount( const double &);
    void setMinPayment( const double &);
    void setInterestRate( const double &);

    int cardCount();

    void printDebtItem();

signals:

public slots:

private:
    QString m_name;
    QString m_color;
    double m_loanAmt;
    double m_minPayment;
    double m_interestRate;
};

#endif // DEBTITEM_H
