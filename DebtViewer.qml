import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2

Rectangle {
    anchors.fill: parent

    GridView {
        anchors.top: parent.top
        anchors.bottom: paymentButton.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: 15

        model: root.currentDebtCardCount
        clip: true
        cellHeight: 40
        cellWidth: 40
        delegate: Pane {
            width: 30
            height: 30

            Material.elevation: 6
            Material.background: {
                root.currentDebtColor
            }
        }
    }

    Button {
        id: settingsButton
        anchors.bottom: parent.bottom
        anchors.bottomMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Edit")
        onClicked: {
            editDebtWindow.visible = true
        }
    }

    Button {
        id: paymentButton
        highlighted: true
        anchors.bottom: settingsButton.top
        anchors.bottomMargin: 20
        anchors.horizontalCenter: parent.horizontalCenter
        text: qsTr("Make Payment")
        onClicked: {
            paymentWindow.visible = true
        }
    }

    EditDebtWindow{
        id: editDebtWindow
        visible: false
    }

    PaymentWindow {
        id: paymentWindow
        visible: false
    }


}
