import QtQuick 2.0
import QtQuick.Controls 2.2
import QtQuick.Controls.Material 2.2

Rectangle {
    id: paymentWindow
    visible: false
    anchors.fill: parent
    color: Qt.rgba( 0, 0, 0, .5)

    onVisibleChanged: {
        if ( visible ) {
            paymentInnerRect.height = height * .5
            paymentInnerRect.width = width * .75
        }
        else {
            paymentInnerRect.height = 0
            paymentInnerRect.width = 0
        }
    }

    Rectangle {
        id: paymentInnerRect
        anchors.centerIn: parent
        radius: 5
        color: "white"

        Label {
            id: enterPaymentLabel
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: parent.top
            anchors.topMargin: 20
            text: qsTr("Enter Payment:")
        }

        TextField {
            id: enterPaymentInput
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: enterPaymentLabel.bottom
            anchors.topMargin: 20
            validator: DoubleValidator { bottom: 00.00;
                decimals: 2;
                notation: DoubleValidator.StandardNotation }
            inputMethodHints: Qt.ImhFormattedNumbersOnly
            text: debtManager.minPayment( root.currentDebtName )
            horizontalAlignment: Text.AlignHCenter
        }

        Button {
            id: enterPushButton
            highlighted: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.top: enterPaymentInput.bottom
            anchors.topMargin: 20
            text: qsTr("Enter")
            onClicked: {
                debtManager.makePayment( root.currentDebtIndex, enterPaymentInput.text)
                root.currentDebtCardCount = debtManager.debtItemCardCount( root.currentDebtName )
                paymentWindow.visible = false
                enterPaymentInput.text = ""
            }
        }

        Button {
            id: closePushButton
            Material.accent: Material.Red
            highlighted: true
            anchors.horizontalCenter: parent.horizontalCenter
            anchors.bottom: parent.bottom
            anchors.bottomMargin: 20
            text: qsTr("Close")
            onClicked: {
                paymentWindow.visible = false
                enterPaymentInput.text = ""
            }
        }

        Behavior on height {

           NumberAnimation {
               duration: 500
               easing.type: Easing.OutBounce
           }
        }

        Behavior on width {

           NumberAnimation {
               duration: 500
               easing.type: Easing.OutBounce
           }
       }
    }
}
