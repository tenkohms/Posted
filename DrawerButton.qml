import QtQuick 2.0
import QtQuick.Controls 2.2
Rectangle {
    property string imageSource
    property string buttonText

    id: homeButtonBG
    anchors.left: parent.left
    width: parent.width
    height: parent.height / 15
    color: "white"
    Image {
        id: buttonImage
        anchors.left: parent.left
        anchors.leftMargin: 20
        anchors.verticalCenter: parent.verticalCenter
        height: parent.height * .8
        width: height
        fillMode: Image.PreserveAspectFit
        source: imageSource
    }
    Text {
        id: buttonLabel
        height: buttonImage.height * .8
        anchors.left: buttonImage.right
        anchors.leftMargin: 20
        anchors.right: parent.right
        text: buttonText
        color: "#7d7d7d"
        anchors.verticalCenter: buttonImage.verticalCenter
        fontSizeMode: Text.Fit; minimumPixelSize: 10; font.pixelSize: 1000
    }
    MouseArea{
        anchors.fill: parent
        onPressed: {
            homeButtonBG.color = "#7d7d7d"
            buttonLabel.color = "black"
        }
        onReleased: {
            homeButtonBG.color = "white"
            buttonLabel.color = "#7d7d7d"
        }
    }
}
