#include "debtpersisthandler.h"
#include "settingshandler.h"

DebtPersistHandler::DebtPersistHandler(QObject *parent) : QObject(parent)
{

}

void DebtPersistHandler::saveDebtItem(DebtItem *debtItem)
{
    QString key = debtItem->name();
    QString value = debtItem->color() + ":" + QString::number(debtItem->loanAmt()) + ":" +
                    QString::number( debtItem->minPayment() ) + ":" + QString::number( debtItem->interestRate() );

    SettingsHandler& mSettings = SettingsHandler::getInstance();
    mSettings.SetValue(key, value);
}

void DebtPersistHandler::deleteDebtItem(const QString &debtName)
{
    SettingsHandler& mSettings = SettingsHandler::getInstance();
    mSettings.ClearValue( debtName );
    QString debtList = mSettings.GetValue("DebtList").toString();
    debtList.replace(debtName + ",", "");
    debtList.replace("," + debtName,"");
    mSettings.SetValue("DebtList", debtList);
}

void DebtPersistHandler::updateDebtItem(DebtItem *debtItem)
{
    SettingsHandler& mSettings = SettingsHandler::getInstance();
    mSettings.ClearValue( debtItem->name() );
    saveDebtItem( debtItem );
}

DebtItem* DebtPersistHandler::loadDebtItem(const QString &debtName)
{
    SettingsHandler& mSettings = SettingsHandler::getInstance();

    QStringList values = mSettings.GetValue( debtName ).toString().split(QRegExp(":"));
    DebtItem * nDebtItem = new DebtItem( nullptr, debtName, values[0], QString( values[1] ).toDouble( ), QString( values[2] ).toDouble( ), QString( values[3] ).toDouble( ) );
    return nDebtItem;
}
