#include "debtmanager.h"
#include <QDebug>

#include "settingshandler.h"

DebtManager::DebtManager(QObject *parent, DebtModel * model)
    : QObject(parent),
      m_debtModel( model )
{
    _nameToDebtItemMap.clear();
    SettingsHandler &mSettings = SettingsHandler::getInstance();

    if ( mSettings.ValueExists("DebtList") )
    {
        //mSettings.ClearValue("DebtList");
        LoadDebtList();
    }
}

DebtManager::~DebtManager()
{
    for ( QMap< QString, DebtItem*>::iterator it = _nameToDebtItemMap.begin(); it != _nameToDebtItemMap.end(); it++ )
    {
        DebtItem * oldItem = it.value();
        delete oldItem;
    }
}

void DebtManager::addDebt(const QString &name, const QString &color, const QString &loanAmount, const QString &minPayment, const QString &interestRate)
{
    DebtItem * nDebtItem = new DebtItem( this, name, color, loanAmount.toDouble(), minPayment.toDouble(), interestRate.toDouble() / 1000.0 );
    m_debtModel->addDebt(DebtDisplay( nDebtItem->name(), nDebtItem->color(), nDebtItem->cardCount()) );

    SettingsHandler &mSettings = SettingsHandler::getInstance();
    QString debtList;
    if ( mSettings.ValueExists("DebtList") )
    {
        debtList = mSettings.GetValue("DebtList" ).toString();
        debtList.append(",");
    }
    debtList.append( name );
    mSettings.SetValue("DebtList", debtList);

    persistHandler.saveDebtItem( nDebtItem );
    _nameToDebtItemMap[ name ] = nDebtItem;

}

void DebtManager::removeRows(const int &index)
{
    QString debtName = m_debtModel->debtName( index );
    DebtItem * oldDebtItem = _nameToDebtItemMap[ debtName ];
    m_debtModel->removeDebt( index );
    delete oldDebtItem;
    _nameToDebtItemMap.remove( debtName );
    persistHandler.deleteDebtItem( debtName );
}

void DebtManager::editDebt(const int &index, const QString & name, const QString & color, const QString & loanAmount, const QString & minPayment, const QString & interestRate)
{
    QString oldName = m_debtModel->debtName( index );
    DebtItem * itemToChange = _nameToDebtItemMap[ oldName ];

    itemToChange->editMulti(name, color, loanAmount, minPayment, interestRate);

    if ( oldName != name )
    {
        persistHandler.deleteDebtItem( oldName );
        _nameToDebtItemMap.remove( oldName );
        _nameToDebtItemMap[ itemToChange->name() ] = itemToChange;
        persistHandler.saveDebtItem( itemToChange );
    }
    else
    {
        persistHandler.updateDebtItem( itemToChange );
    }

    m_debtModel->editDebt( index, name, color, itemToChange->cardCount() );
}

void DebtManager::makePayment(const int &index, const QString &paymentAmt)
{
    QString debtName = m_debtModel->debtName( index );
    DebtItem * itemToChange = _nameToDebtItemMap[ debtName ];
    itemToChange->setLoanAmount( itemToChange->loanAmt() - paymentAmt.toDouble() );
    m_debtModel->editDebt( index, debtName, itemToChange->color(), itemToChange->cardCount() );

    persistHandler.updateDebtItem( itemToChange );

}

int DebtManager::debtItemCardCount(const QString &name)
{
    return _nameToDebtItemMap[ name ]->cardCount();
}

QString DebtManager::debtAmount(const QString &name)
{
    return QString::number( _nameToDebtItemMap[ name ]->loanAmt() );
}

QString DebtManager::interestRate(const QString &name)
{
    return QString::number( _nameToDebtItemMap[ name ]->interestRate() * 1000.0 );
}

QString DebtManager::minPayment(const QString &name)
{
    return QString::number( _nameToDebtItemMap[ name ]->minPayment() );
}

void DebtManager::LoadDebtList()
{
    SettingsHandler &mSettings = SettingsHandler::getInstance();
    QStringList debtList = mSettings.GetValue("DebtList").toString().split(QRegExp( "," ) );
    foreach( const QString& debtName, debtList ) {
        DebtItem * nDebtItem = persistHandler.loadDebtItem( debtName );
        //nDebtItem->printDebtItem();
        m_debtModel->addDebt(DebtDisplay( nDebtItem->name(), nDebtItem->color(), nDebtItem->cardCount()) );
        _nameToDebtItemMap[ debtName ] = nDebtItem;
    }

}
