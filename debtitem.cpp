#include "debtitem.h"
#include <QDebug>
#include <math.h>

DebtItem::DebtItem(QObject *parent, QString name, QString color, double loanAmt, double minPayment, double interestRate)
    : m_name( name ),
      m_color( color ),
      m_loanAmt( loanAmt ),
      m_minPayment( minPayment ),
      m_interestRate( interestRate )
{

}

QString DebtItem::name()
{
    return m_name;
}

QString DebtItem::color()
{
    return m_color;
}

double DebtItem::loanAmt()
{
    return m_loanAmt;
}

double DebtItem::minPayment()
{
    return m_minPayment;
}

double DebtItem::interestRate()
{
    return m_interestRate;
}

void DebtItem::editMulti(const QString &name, const QString &color, const QString &loanAmount, const QString &minPayment, const QString &interestRate)
{
    m_name = name;
    m_color = color;
    m_loanAmt = loanAmount.toDouble();
    m_minPayment = minPayment.toDouble();
    m_interestRate = interestRate.toDouble()/ 1000.0;
}

int DebtItem::cardCount()
{
    double calculatedRepaymentNumber = ( -log( 1.0 - m_interestRate * m_loanAmt / m_minPayment ) ) / log( 1.0 + m_interestRate);
    int ret = int( calculatedRepaymentNumber );
    if( calculatedRepaymentNumber - double( ret ) != 0 )
        ret += 1;
    return ret;
}

void DebtItem::setName(const QString &name)
{
    m_name = name;
}

void DebtItem::setColor(const QString &color )
{
    m_color = color;
}

void DebtItem::setLoanAmount(const double & loanAmount)
{
    m_loanAmt = loanAmount;
}

void DebtItem::setMinPayment(const double &minPayment)
{
    m_minPayment = minPayment;
}

void DebtItem::setInterestRate(const double &interestRate)
{
    m_interestRate = interestRate;
}

void DebtItem::printDebtItem()
{
    qDebug() << "Name: " << m_name;
    qDebug() << "color: " << m_color;
    qDebug() << "amount: " << m_loanAmt;
    qDebug() << "minPayment: " << m_minPayment;
    qDebug() << "interest: " << m_interestRate;
}
